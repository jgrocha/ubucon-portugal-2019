# E que tal usar o Ubuntu para processar imagens de um Drone?

## Download workshop materials

All the workshop contents is stored in a git repository. Please install `git` to get the workshop data.

After installing git, please run:

```
git clone https://gitlab.com/jgrocha/ubucon-portugal-2019
```

You will get a new folder called `ubucon-portugal-2019`.

## Installing QGIS

https://www.qgis.org

### Installing QGIS Plugins

1. ImportPhotos
2. MapSwipe
3. Profile tool

## The initial data

Open QGIS.

### Images

Use the Import Photo plugin to load the image location. 

1. Style the images with the image name as label. 
1. Style the RelPath field as an Attachment

### Ground Control Points (GCP)

Use the Add delimited text to add the ground control points.

1. Style the GCP with its number as label.
1. STyle the GCP as circles with only strike color (transparent filling).

### Flight path

Use the points to path processing to create the drone path.

1. Use Points to path processing algoritm

## Install WebODM

To install WebODM, we will need:

1. Docker
2. Python

### Install Docker (from the Official Docker Repository)

Instruções: https://www.digitalocean.com/community/tutorials/como-instalar-e-usar-o-docker-no-ubuntu-18-04-pt

Add the Official Docker Repository

```
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt policy docker-ce
```

Install

```
sudo apt install docker-ce docker-compose
```

Run without sudo

```
sudo usermod -aG docker ${USER}
su - ${USER}
```

### Install Python (for Linux users)

```
sudo apt install python-setuptools
```

## Test docker installation

```
docker run hello-world
```

```
docker search wordpress
sudo docker run 9000:80 wordpress
```

# Run WebODM

## Run WebODM using the command line script webodm.sh

[WebODM home page](https://github.com/OpenDroneMap/WebODM)

Use the shell (Linux/OSX) or the Power shell (windows) to run:

```
git clone https://github.com/OpenDroneMap/WebODM --config core.autocrlf=input --depth 1
cd WebODM
./webodm.sh update
```

It will take some time to download the WebODM image. Wait until the image if fully downloaded.

```
./webodm.sh start
```

After the service start, you can use WebODM from: http://localhost:8000

## Run WebODM diretly using the docker (untested)

You can run WebODM diretly using docker, using the image already stored in the docker hub.

```bash
docker run splashblot/webodm
```

### New WebODM project

### New task

### Explore the results within WebODM

### Open results in QGIS
